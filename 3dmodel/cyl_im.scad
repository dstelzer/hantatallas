use <../3dmodel/cuneiform.scad>

use <../3dmodel/cylinder.scad>

cylindrify(101.33333333333333, 15, 1.5, 200)
rotate([0,0,90])
translate([101.33333333333333, 0, 0])
scale([1,-1,1]){
	rotate([0,180,0])
	union(){
		cube([101.33333333333333,15,2]);
		difference(){
			union(){
				edgerule(0, 101.333, 0.5, true);
				edgerule(15, 101.333, 0.5, false);
				vrule(99.66666666666666, 15, 0.5);
				translate([2.5,2.5,0]){
					scale([10.0,10.0,10.0]){
						translate([0,0,0]){
							rotate(-90.0){
								singlestroke(-0.6666666666666667, 0, 0.3333333333333333, 1);
							}
							hookstroke(0.4166666666666667, 0.16666666666666669, 0.3333333333333333, 0.6666666666666666);
							hookstroke(0.75, 0.0, 0.25, 0.5);
							hookstroke(0.75, 0.5, 0.25, 0.5);
						}
						translate([1.5,0,0]){
							hookstroke(0.0, 0.0, 0.3333333333333333, 0.6666666666666666);
							hookstroke(0.5416666666666665, 0.0, 0.08333333333333333, 0.16666666666666666);
							rotate(-90.0){
								singlestroke(-0.2667181069958848, 0.33333333333333326, 0.19997427983539096, 0.4999999999999999);
							}
							rotate(-90.0){
								singlestroke(-0.46669238683127573, 0.33333333333333326, 0.19997427983539096, 0.4999999999999999);
							}
							rotate(-90.0){
								singlestroke(-0.6666666666666667, 0.33333333333333326, 0.19997427983539096, 0.4999999999999999);
							}
							rotate(-90.0){
								singlestroke(-1.0, 0, 0.3333333333333333, 0.8333333333333333);
							}
							singlestroke(0.6666666666666666, 0, 0.3333333333333333, 1);
						}
						translate([2.625,0,0]){
							rotate(-90.0){
								singlestroke(-0.6666666666666667, 0, 0.3333333333333333, 0.8181423611111112);
							}
							singlestroke(0.681857638888889, 0.0, 0.2727141203703704, 0.5);
							singlestroke(0.9545717592592593, 0.0, 0.2727141203703704, 0.5);
							singlestroke(1.2272858796296298, 0.0, 0.2727141203703704, 0.5);
							singlestroke(0.9242621527777779, 0.5, 0.3333333333333333, 0.5);
						}
						translate([4.25,0,0]){
							rotate(-90.0){
								singlestroke(-0.25, 0.0, 0.25, 0.75);
							}
							rotate(-90.0){
								singlestroke(-0.5, 0.0, 0.25, 0.75);
							}
							rotate(-90.0){
								singlestroke(-0.75, 0.0, 0.25, 0.75);
							}
							rotate(-90.0){
								singlestroke(-1.0, 0.0, 0.25, 0.75);
							}
							singlestroke(0.7708333333333334, 0, 0.3333333333333333, 0.8333333333333333);
							singlestroke(1.1458333333333333, 0, 0.3333333333333333, 0.8333333333333333);
							rotate(-90.0){
								singlestroke(-1.0, 0.75, 0.3333333333333333, 0.75);
							}
						}
						translate([6.25,0,0]){
							hookstroke(0.04166666666666666, 0.0, 0.25, 0.5);
							hookstroke(0.04166666666666666, 0.5, 0.25, 0.5);
							singlestroke(0.3333333333333333, 0, 0.3333333333333333, 1);
						}
						translate([7.041666666666667,0,0]){
							singlestroke(0, 0, 0.3333333333333333, 1);
							rotate(-90.0){
								singlestroke(-0.6666666666666667, 0.16666666666666666, 0.3333333333333333, 0.49999999999999994);
							}
						}
						translate([7.833333333333334,0,0]){
							rotate(-90.0){
								singlestroke(-0.3333333333333333, 0, 0.3333333333333333, 1.3333333333333333);
							}
							rotate(-90.0){
								doublestroke(-0.6666666666666666, 0, 0.3333333333333333, 1.1666666666666665);
							}
							hookstroke(1.0833333333333333, 0.3333333333333333, 0.25, 0.3333333333333333);
							rotate(-90.0){
								singlestroke(-1.0, 0, 0.3333333333333333, 1.3333333333333333);
							}
							singlestroke(1.1666666666666665, 0, 0.3333333333333333, 1);
						}
					}
				}
			}
			cube([101.33333333333333,15,100]);
		}
	}
}
